# README #

# **Workforce Server Android App** #

Workforce Server Android app act as a server which will receive messages from the client app. Once the app is launched it will continue to monitor(using broadcast receiver) the messages received and if the received message is in correct format it is stored in the local DB with mobile number. 

If already DB has some values, the mobile number is populated in the spinner and the corresponding details(CellID, Location Area Network, Mobile Country Code and Mobile Network code) are fetched from the DB and using Google Geolocation API the latitude and longitude are fetched.

Using Google Geocoder API address is fetched from the latitude and longitude received from the GeoLocation API and populated in the RecylerView / MapView.

## UI ELEMENTS USED ##

* Spinner -> Populates the mobile numbers
* RecylerView -> Populates the date and time and address of the location
* MapView -> Markers are added on the location
* Refresh button -> Refreshes the values and fetched the data freshly
* Flip View button -> Flips between RecylerView/Mapview

## External Libraries Used ##

1. Android Support Libraries (**Appcompat-v7:22.2.1, RecyclerView-v7:22.2.1, CardView-v7:22.1**)  

2. VolleyLibrary:1.0.19 - For communicating with the backend. 

3. GooglePlayServices:7.8.0

4. Eventbus:2.4.0 - Its an open source library that publish/subscribe event bus optimized for Android. (https://github.com/greenrobot/EventBus)

### Tested Mobile ###

* Nexus 5 (Android 6.0.1)