package com.biztalk.workforceserver.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hl0395 on 13/1/16.
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "WorkforceDB.db";
    public static final String WORKFORCE_TABLE_NAME = "workforce";
    public static final String WORKFORCE_COLUMN_ID = "id";
    public static final String WORKFORCE_COLUMN_CARRIER = "carrier";
    public static final String WORKFORCE_COLUMN_CELLID = "cellid";
    public static final String WORKFORCE_COLUMN_LAC = "lac";
    public static final String WORKFORCE_COLUMN_MCC = "mcc";
    public static final String WORKFORCE_COLUMN_MNC = "mnc";
    public static final String WORKFORCE_COLUMN_TIMESTAMP = "timestamp";

    /**
     * Constructor
     * @param context is the activity/fragmet context
     */
    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    /**
     * Called when the database is created for the first time. This is where the
     * creation of tables and the initial population of the tables should happen.
     *
     * @param db The database.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + WORKFORCE_TABLE_NAME +
                        " (" + WORKFORCE_COLUMN_ID + " long, " + WORKFORCE_COLUMN_CARRIER + " text," +
                        WORKFORCE_COLUMN_CELLID + " text," + WORKFORCE_COLUMN_LAC + " text, " +
                        WORKFORCE_COLUMN_MCC + " text," + WORKFORCE_COLUMN_MNC + " text," +
                        WORKFORCE_COLUMN_TIMESTAMP + " text)"
        );
    }

    /**
     * Called when the database needs to be upgraded. The implementation
     * should use this method to drop tables, add tables, or do anything else it
     * needs to upgrade to the new schema version.
     * <p/>
     * <p>
     * The SQLite ALTER TABLE documentation can be found
     * <a href="http://sqlite.org/lang_altertable.html">here</a>. If you add new columns
     * you can use ALTER TABLE to insert them into a live table. If you rename or remove columns
     * you can use ALTER TABLE to rename the old table, then create the new table and then
     * populate the new table with the contents of the old table.
     * </p><p>
     * This method executes within a transaction.  If an exception is thrown, all changes
     * will automatically be rolled back.
     * </p>
     *
     * @param db         The database.
     * @param oldVersion The old database version.
     * @param newVersion The new database version.
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+WORKFORCE_TABLE_NAME);
        onCreate(db);
    }

    /**
     *  Function to insert a row in the DB
     * @param mobNo is the mobile number of the client
     * @param carrier is the carrier network to which client is connected
     * @param cellID is the cellID obtained for the client
     * @param lac is the location area network
     * @param mcc is the mobile country code
     * @param mnc is the mobile network code
     * @param timestamp is the timestamp in millisecond
     * @return true iff record inserted successfully else false
     */
    public boolean insertWorkforceData(long mobNo, String carrier, String cellID, String lac,
                                       String mcc, String mnc, String timestamp){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(WORKFORCE_COLUMN_ID, mobNo);
        contentValues.put(WORKFORCE_COLUMN_CARRIER, carrier);
        contentValues.put(WORKFORCE_COLUMN_CELLID, cellID);
        contentValues.put(WORKFORCE_COLUMN_LAC, lac);
        contentValues.put(WORKFORCE_COLUMN_MCC, mcc);
        contentValues.put(WORKFORCE_COLUMN_MNC, mnc);
        contentValues.put(WORKFORCE_COLUMN_TIMESTAMP, timestamp);
        db.insert(WORKFORCE_TABLE_NAME, null, contentValues);
        return true;
    }

    /**
     * Function to get all the columns of the table
     * @param mobileNo is the mobile number of the client
     * @return the cursor object the contains the query result
     */
    public Cursor getData(long mobileNo){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from "+ WORKFORCE_TABLE_NAME +" where id="+mobileNo, null );
        return res;
    }

    /**
     * Function to get all the mobile number fields in the table
     * @return the cursor object the contains the query result
     */
    public Cursor getAllMobileNos(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select "+WORKFORCE_COLUMN_ID+" from "+ WORKFORCE_TABLE_NAME, null );
        return res;
    }

}

