package com.biztalk.workforceserver.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by hl0395 on 14/1/16.
 */
public class WorkforceServerConstants {

    public static final int GET_DATE_WITH_TIME = 0;
    public static final int GET_DATE = 1;
    public static final int GET_TIME = 2;


    public static final String CELL_ID = "cellId";
    public static final String LAC = "locationAreaCode";
    public static final String MCC = "mobileCountryCode";
    public static final String MNC = "mobileNetworkCode";
    public static final String CELL_TOWER = "cellTowers";

    /**
     * Events constants
     */
    public static final String CLIENT_SELECTED = "ClientSelected";
    public static final String FLIP_VIEW = "FlipView";
    public static final String CLEAR_VIEW = "ClearView";

    /**
     * Function to return the date in a specified format
     * @param timeStamp is the millisecond
     * @param type what type of date/time format
     * @return the date in specified format
     */
    public static String getDate(long timeStamp, int type){
        try{
            DateFormat sdf = null;
            switch (type){
                case GET_DATE_WITH_TIME:
                    sdf = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
                    break;
                case GET_DATE:
                    sdf = new SimpleDateFormat("dd-MMM-yyyy");
                    break;
                case GET_TIME:
                    sdf = new SimpleDateFormat("HH:mm:ss");
                    break;
            }
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return null;
    }

    /**
     * Function that checks for internet connection
     * @param mContext is the context
     * @return true if internet connection available or false
     */
    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
