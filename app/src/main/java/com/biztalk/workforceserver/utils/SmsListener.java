package com.biztalk.workforceserver.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.biztalk.workforceserver.db.DBHelper;

import org.json.JSONObject;

/**
 * Created by hl0395 on 13/1/16.
 */
public class SmsListener extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub

        if(intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")){
            Bundle bundle = intent.getExtras();
            SmsMessage[] msgs = null;
            String msg_from;
            if (bundle != null){
                try{
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for(int i=0; i<msgs.length; i++){
                        msgs[i] = SmsMessage.createFromPdu((byte[])pdus[i]);
                        msg_from = msgs[i].getOriginatingAddress();
                        String msgBody = msgs[i].getMessageBody();
                        long timestamp = msgs[i].getTimestampMillis();
                        insertIntoDB(context, Long.parseLong(msg_from), msgBody,timestamp );
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void insertIntoDB(Context context, long mobNo, String msgJson, long timeStamp) {

        try {
            JSONObject msg = new JSONObject(msgJson);

            DBHelper dbHelper = new DBHelper(context);
            boolean status = dbHelper.insertWorkforceData(mobNo, msg.getString("carrier"), msg.getString("cellID"),
                    msg.getString("LAC"), msg.getString("mcc"), msg.getString("mnc"),timeStamp+"");
            Log.d("TAG","status is --- "+status+"---"+mobNo);
            dbHelper.close();

        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
