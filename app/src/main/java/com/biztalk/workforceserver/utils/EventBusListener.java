package com.biztalk.workforceserver.utils;

/**
 * Created by hl0395 on 13/1/16.
 */
public class EventBusListener {
    private String type;
    private Object data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
