package com.biztalk.workforceserver.main;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.biztalk.workforceserver.R;
import com.biztalk.workforceserver.db.DBHelper;
import com.biztalk.workforceserver.utils.EventBusListener;
import com.biztalk.workforceserver.utils.WorkforceServerConstants;

import java.util.ArrayList;
import java.util.HashSet;

import de.greenrobot.event.EventBus;

public class MainActivity extends AppCompatActivity {

    DBHelper dbHelper;
    Spinner mWorkSpaceSpinner;
    ArrayAdapter<String> mWorkspaceSpinAdapter;
    HashSet<String> mSpinnerItems = new HashSet<>();
    ArrayList<String> spinnerArray;

    /**
     * Dispatch onStart() to all fragments.  Ensure any created loaders are
     * now started.
     */
    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper =  new DBHelper(this);

        setupViews();
        getAllMobileNumbers();
        addMainFragment();
    }

    /**
     * Function to initialize and set up all the views from the layout
     */
    private void setupViews() {
        mWorkSpaceSpinner = (Spinner) findViewById(R.id.client_spinner);
    }

    /**
     * Function to add the main fragment in the activity
     */
    private void addMainFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MainFragment mainFragment = new MainFragment();
        fragmentTransaction.add(R.id.frame_container, mainFragment);
        fragmentTransaction.commit();
    }

    /**
     * Function is called when is event is received
     * @param eventBusListener
     */
    public void onEvent(EventBusListener eventBusListener){}

    /**
     * Function to fetch all the mobile numbers from the DB and set the spinner adapter
     */

    private void getAllMobileNumbers(){

        EventBusListener eventBusListener = new EventBusListener();
        eventBusListener.setType(WorkforceServerConstants.CLEAR_VIEW);
        EventBus.getDefault().post(eventBusListener);

        mSpinnerItems.clear();
        Cursor cursor = dbHelper.getAllMobileNos();
        if (cursor.moveToFirst()) {
            do {
                mSpinnerItems.add(cursor.getString(0));

            } while (cursor.moveToNext());
        }
        spinnerArray = new ArrayList<>();
        spinnerArray.addAll(mSpinnerItems);

        mWorkspaceSpinAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
                    spinnerArray);
        mWorkspaceSpinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mWorkSpaceSpinner.setAdapter(mWorkspaceSpinAdapter);

        mWorkSpaceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EventBusListener eventBusListener = new EventBusListener();
                eventBusListener.setType(WorkforceServerConstants.CLIENT_SELECTED);
                eventBusListener.setData(Long.parseLong(spinnerArray.get(position)));
                EventBus.getDefault().post(eventBusListener);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Function to flip the RecylerView / SupportMapFragment
     */
    private void flipView() {
        EventBusListener eventBusListener = new EventBusListener();
        eventBusListener.setType(WorkforceServerConstants.FLIP_VIEW);
        EventBus.getDefault().post(eventBusListener);
    }

    /**
     * Initialize the contents of the Activity's standard options menu.  You
     * should place your menu items in to <var>menu</var>.
     * <p/>
     * <p>This is only called once, the first time the options menu is
     * displayed.  To update the menu every time it is displayed, see
     * {@link #onPrepareOptionsMenu}.
     * <p/>
     * <p>The default implementation populates the menu with standard system
     * menu items.  These are placed in the {@link Menu#CATEGORY_SYSTEM} group so that
     * they will be correctly ordered with application-defined menu items.
     * Deriving classes should always call through to the base implementation.
     * <p/>
     * <p>You can safely hold on to <var>menu</var> (and any items created
     * from it), making modifications to it as desired, until the next
     * time onCreateOptionsMenu() is called.
     * <p/>
     * <p>When you add items to the menu, you can implement the Activity's
     * {@link #onOptionsItemSelected} method to handle them there.
     *
     * @param menu The options menu in which you place your items.
     * @return You must return true for the menu to be displayed;
     * if you return false it will not be shown.
     * @see #onPrepareOptionsMenu
     * @see #onOptionsItemSelected
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * The default implementation simply returns false to have the normal
     * processing happen (calling the item's Runnable or sending a message to
     * its Handler as appropriate).  You can use this method for any items
     * for which you would like to do processing without those other
     * facilities.
     * <p/>
     * <p>Derived classes should call through to the base class for it to
     * perform the default menu handling.</p>
     *
     * @param item The menu item that was selected.
     * @return boolean Return false to allow normal menu processing to
     * proceed, true to consume it here.
     * @see #onCreateOptionsMenu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            getAllMobileNumbers();
        }else if(id == R.id.action_flip)
            flipView();

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

}
