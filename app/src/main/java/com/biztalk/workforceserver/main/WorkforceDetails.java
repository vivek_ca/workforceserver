package com.biztalk.workforceserver.main;

/**
 * Created by hl0395 on 13/1/16.
 */
public class WorkforceDetails {

    public String mMobileNo;
    public String mCarrier;
    public String mCellID;
    public String mLAC;
    public String mMcc;
    public String mMnc;
    public String mTimestamp;
    public String mAddress;

    public String getmMobileNo() {
        return mMobileNo;
    }

    public void setmMobileNo(String mMobileNo) {
        this.mMobileNo = mMobileNo;
    }

    public String getmCarrier() {
        return mCarrier;
    }

    public void setmCarrier(String mCarrier) {
        this.mCarrier = mCarrier;
    }

    public String getmCellID() {
        return mCellID;
    }

    public void setmCellID(String mCellID) {
        this.mCellID = mCellID;
    }

    public String getmLAC() {
        return mLAC;
    }

    public void setmLAC(String mLAC) {
        this.mLAC = mLAC;
    }

    public String getmMcc() {
        return mMcc;
    }

    public void setmMcc(String mMcc) {
        this.mMcc = mMcc;
    }

    public String getmMnc() {
        return mMnc;
    }

    public void setmMnc(String mMnc) {
        this.mMnc = mMnc;
    }

    public String getmTimestamp() {
        return mTimestamp;
    }

    public void setmTimestamp(String mTimestamp) {
        this.mTimestamp = mTimestamp;
    }

    public String getmAddress() {
        return mAddress;
    }

    public void setmAddress(String mAddress) {
        this.mAddress = mAddress;
    }
}
