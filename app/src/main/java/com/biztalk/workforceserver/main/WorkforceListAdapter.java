package com.biztalk.workforceserver.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.biztalk.workforceserver.R;
import com.biztalk.workforceserver.utils.WorkforceServerConstants;

import java.util.ArrayList;

/**
 * Created by hl0395 on 13/1/16.
 */
public class WorkforceListAdapter extends RecyclerView.Adapter<WorkforceListAdapter.ViewHolder> {

    /**
     * Constains the raw data
     */
    private ArrayList<WorkforceDetails> mDataSet;


    /**
     * ViewHolder class loads the views for the Recyler view item.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView mWorkforceDate;
        public TextView mWorkforceAddress;
        public ProgressBar mLoadingProgress;

        /**
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            mWorkforceDate = (TextView)itemView.findViewById(R.id.workforce_date);
            mWorkforceAddress = (TextView)itemView.findViewById(R.id.workforce_address);
            mLoadingProgress = (ProgressBar)itemView.findViewById(R.id.progress_bar);
        }
    }

    /**
     * getter function for mDataSet
     *
     * @return the ArrayList containing the values
     */
    public ArrayList<WorkforceDetails> getmDataSet() {
        return mDataSet;
    }

    /**
     * setter function for mDataSet
     */

    public void setmDataSet(ArrayList<WorkforceDetails> mDataSet) {
        this.mDataSet = mDataSet;
    }


    /**
     * Returns the total number of items in the data set hold by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return this.mDataSet != null ? this.mDataSet.size() : 0;
    }

    /**
     * Called when RecyclerView needs a new {@link ViewHolder} of the given type to represent
     * an item.
     * <p/>
     * This new ViewHolder should be constructed with a new View that can represent the items
     * of the given type. You can either create a new View manually or inflate it from an XML
     * layout file.
     * <p/>
     * The new ViewHolder will be used to display items of the adapter using
     * {@link #onBindViewHolder(ViewHolder, int)} (ViewHolder, int, List)}. Since it will be re-used to display
     * different items in the data set, it is a good idea to cache references to sub views of
     * the View to avoid unnecessary {@link View#findViewById(int)} calls.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workforce_list_item, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    /**
     * Called by RecyclerView to display the data at the specified position. This method should
     * update the contents of the {@link ViewHolder#itemView} to reflect the item at the given
     * position.
     * <p/>
     * Note that unlike {@link ListView}, RecyclerView will not call this method
     * again if the position of the item changes in the data set unless the item itself is
     * invalidated or the new position cannot be determined. For this reason, you should only
     * use the <code>position</code> parameter while acquiring the related data item inside
     * this method and should not keep a copy of it. If you need the position of an item later
     * on (e.g. in a click listener), use {@link ViewHolder#getAdapterPosition()} which will
     * have the updated adapter position.
     * <p/>
     * Override {@link #onBindViewHolder(ViewHolder, int)} (ViewHolder, int, List)} instead if Adapter can
     * handle effcient partial bind.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final WorkforceDetails workforce = mDataSet.get(position);

        holder.mWorkforceDate.setText(WorkforceServerConstants.getDate(Long.parseLong(workforce.getmTimestamp()),
                WorkforceServerConstants.GET_DATE_WITH_TIME));

        holder.mWorkforceAddress.setText(workforce.getmAddress());

        if(workforce.getmAddress() != null && workforce.getmAddress().trim().length() > 0)
            holder.mLoadingProgress.setVisibility(View.GONE);
        else
            holder.mLoadingProgress.setVisibility(View.VISIBLE);
    }

}
