package com.biztalk.workforceserver.main;

import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.biztalk.workforceserver.R;
import com.biztalk.workforceserver.db.DBHelper;
import com.biztalk.workforceserver.utils.EventBusListener;
import com.biztalk.workforceserver.utils.WorkforceServerConstants;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * Created by hl0395 on 13/1/16.
 */
public class MainFragment extends Fragment {

    View mMainView;
    RecyclerView mWOrkForceList;
    RequestQueue mRequestQueue;
    ArrayList<WorkforceDetails> mworkForceList;
    WorkforceListAdapter workforceListAdapter;
    DBHelper dbHelper;
    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    JsonObjectRequest request;

    /**
     * Called to have the fragment instantiate its user interface view.
     * This is optional, and non-graphical fragments can return null (which
     * is the default implementation).  This will be called between
     * {@link #onCreate(Bundle)} and {@link #onActivityCreated(Bundle)}.
     * <p/>
     * <p>If you return a View from here, you will later be called in
     * {@link #onDestroyView} when the view is being released.
     *
     * @param inflater           The LayoutInflater object that can be used to inflate
     *                           any views in the fragment,
     * @param container          If non-null, this is the parent view that the fragment's
     *                           UI should be attached to.  The fragment should not add the view itself,
     *                           but this can be used to generate the LayoutParams of the view.
     * @param savedInstanceState If non-null, this fragment is being re-constructed
     *                           from a previous saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mMainView = inflater.inflate(R.layout.fragment_main,null);
        setupViews();
        setUpMapIfNeeded();
        return mMainView;
    }

    /**
     * Called when the Fragment is visible to the user.  This is generally
     * tied to {@link android.app.Activity#onStart() Activity.onStart} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    /**
     * Called when the Fragment is no longer started.  This is generally
     * tied to {@link android.app.Activity#onStop() Activity.onStop} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    /**
     * Function to initialize and set up all the views from the layout
     */
    private void setupViews(){
        mRequestQueue = Volley.newRequestQueue(getActivity());
        mworkForceList = new ArrayList<>();

        mWOrkForceList = (RecyclerView) mMainView.findViewById(R.id.workforce_list);
        mWOrkForceList.setHasFixedSize(true);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mWOrkForceList.setLayoutManager(mLayoutManager);

        workforceListAdapter = new WorkforceListAdapter();
        workforceListAdapter.setmDataSet(mworkForceList);
        mWOrkForceList.setAdapter(workforceListAdapter);

        dbHelper =  new DBHelper(getActivity());

    }

    /**
     * Function is called when is event is received
     * @param eventBusListener
     */
    public void onEvent(EventBusListener eventBusListener){
        if(eventBusListener.getType().equals(WorkforceServerConstants.CLIENT_SELECTED)){
            if(WorkforceServerConstants.isNetworkAvailable(getActivity())) {
                mworkForceList.clear();
                workforceListAdapter.notifyDataSetChanged();
                mMap.clear();
                getWorkForceLocation((Long) eventBusListener.getData());
            }else
                Toast.makeText(getActivity(),getString(R.string.internet_connection),Toast.LENGTH_SHORT).show();
        }else if(eventBusListener.getType().equals(WorkforceServerConstants.FLIP_VIEW)){
            if(mMapFragment.getView().getVisibility() == View.VISIBLE){
                mMapFragment.getView().setVisibility(View.GONE);
                mWOrkForceList.setVisibility(View.VISIBLE);
            }else {
                mMapFragment.getView().setVisibility(View.VISIBLE);
                mWOrkForceList.setVisibility(View.GONE);
            }
        }else if(eventBusListener.getType().equals(WorkforceServerConstants.CLEAR_VIEW)){
            mworkForceList.clear();
            workforceListAdapter.notifyDataSetChanged();
            mMap.clear();
        }
    }

    /**
     * Function to setup the SupportMap fragment and initialize the google map
     */
    public void setUpMapIfNeeded() {
        if (mMap == null) {
            mMapFragment = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.location_map));
            mMap = mMapFragment.getMap();
            if (mMap != null)
                mMap.setMyLocationEnabled(true);
        }
    }

    /**
     * Function to fetch all the values from the DB and set the pojo class and
     * refresh the recyler adapter
     * @param mobileNo is the mobile number for which data has to feched from DB
     */
    private void getWorkForceLocation(long mobileNo){
        Cursor cursor = dbHelper.getData(mobileNo);
        if (cursor.moveToFirst()) {
            do {
                WorkforceDetails workforceDetails = new WorkforceDetails();
                workforceDetails.setmMobileNo(cursor.getString(0));
                workforceDetails.setmCarrier(cursor.getString(1));
                workforceDetails.setmCellID(cursor.getString(2));
                workforceDetails.setmLAC(cursor.getString(3));
                workforceDetails.setmMcc(cursor.getString(4));
                workforceDetails.setmMnc(cursor.getString(5));
                workforceDetails.setmTimestamp(cursor.getString(6));

                mworkForceList.add(workforceDetails);
                getLocationRequest(workforceDetails);

            } while (cursor.moveToNext());
        }
        workforceListAdapter.notifyDataSetChanged();
    }

    /**
     * Function to get the location cordinates using the fields obtained from client
     * @param workforceDetails is the client details object
     */
    private void getLocationRequest(final WorkforceDetails workforceDetails){

        String url = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + getString(R.string.geolocation_api_key);

        request = new JsonObjectRequest(Request.Method.POST, url, addJSONParameters(workforceDetails),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            JSONObject location = response.getJSONObject("location");

                            String address = locationToAddress(location.getString("lat"), location.getString("lng"));

                            workforceDetails.setmAddress(address);
                            workforceListAdapter.notifyDataSetChanged();

                            mMap.addMarker(new MarkerOptions().position(new LatLng(
                                    Double.parseDouble(location.getString("lat")),
                                    Double.parseDouble(location.getString("lng")))).
                                    title(WorkforceServerConstants.getDate(Long.parseLong(workforceDetails.getmTimestamp()),
                                            WorkforceServerConstants.GET_DATE)).
                                    snippet(WorkforceServerConstants.getDate(Long.parseLong(workforceDetails.getmTimestamp()),
                                            WorkforceServerConstants.GET_TIME))).showInfoWindow();
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
                                    Double.parseDouble(location.getString("lat")),
                                    Double.parseDouble(location.getString("lng"))), 12.0f));

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        mRequestQueue.add(request);

    }

    /**
     * Function to initialize the JSON request for getting the location
     * @param workforceDetails is the details about the workforce as object
     * @return the json with all the fields required
     */
    private JSONObject addJSONParameters(WorkforceDetails workforceDetails){
        try {
            JSONObject jsonObject = new JSONObject();

            JSONArray jsonArray = new JSONArray();

            JSONObject cellTowerObj = new JSONObject();
            cellTowerObj.put(WorkforceServerConstants.CELL_ID, workforceDetails.getmCellID());
            cellTowerObj.put(WorkforceServerConstants.LAC, workforceDetails.getmLAC());
            cellTowerObj.put(WorkforceServerConstants.MCC, workforceDetails.getmMcc());
            cellTowerObj.put(WorkforceServerConstants.MNC, workforceDetails.getmMnc());
            jsonArray.put(cellTowerObj);

            jsonObject.put(WorkforceServerConstants.CELL_TOWER, jsonArray);

            return  jsonObject;
        }catch (JSONException e){
            e.printStackTrace();
        }
        return null;
    }
    String result = "";

    /**
     * Function uses Geocoder api to convert the location to address
     * @param lat is the latitude
     * @param lng is the longitude
     * @return the address as string
     */
    private String locationToAddress(final String lat, final String lng) {

        new AsyncTask<String, String, String>(

        ) {
            @Override
            protected String doInBackground(String... params) {
                List<Address> addresses = null;
                Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
                String errorMessage = "";
                try {
                    addresses = geocoder.getFromLocation(
                            Double.parseDouble(params[0]),
                            Double.parseDouble(params[1]),
                            1);
                } catch (IOException ioException) {
                    errorMessage = getString(R.string.service_not_available);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = getString(R.string.invalid_lat_long_used);
                }

                if (addresses == null || addresses.size()  == 0) {
                    if (errorMessage.isEmpty()) {
                        errorMessage = getString(R.string.no_address_found);
                    }
                } else {
                    Address address = addresses.get(0);
                    ArrayList<String> addressFragments = new ArrayList<String>();
                    String add = "";
                    for(int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                        addressFragments.add(address.getAddressLine(i));
                        add += address.getAddressLine(i)+",";
                    }


                    return add;
                }

            return errorMessage;

            }

            @Override
            protected void onPostExecute(String s) {
                result = s;
            }
        }.execute(lat, lng);
        return result;
    }


}
